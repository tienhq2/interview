
import anh47 from "./assets/images/items/Rectangle 47.png";
import anh49 from "./assets/images/items/Rectangle 49.png";
import anh51 from "./assets/images/items/Rectangle 51.png";
import anh53 from "./assets/images/items/Rectangle 53.png";
import anh55 from "./assets/images/items/Rectangle 55.png";
import anh57 from "./assets/images/items/Rectangle 57.png";

const items = [
    {
        id:1,
        name: "Leather blog",
        description: "Distressed Tote Leather Blog",
        price: "216,27 $",
        imageUrl: anh47
    },
    {
        id:2,
        name: "Elastic Straps",
        description: "Womens Cognac Leather",
        price: "195,13 $",
        imageUrl: anh49
    },
    {
        id:3,
        name: "Leather blog",
        description: "Distressed Tote Leather Blog",
        price: "910,76 $",
        imageUrl: anh51
    },
    {
        id:4,
        name: "",
        description: "High Top Canvas Shoes",
        price: "341,02 $",
        imageUrl: anh53
    },
    {
        id:5,
        name: "Wedding Ring Set",
        description: "Rose Gold Moissanite Vintage",
        price: "3,341,13 $",
        imageUrl: anh55
    },
    {
        id:6,
        name: "Plated Handmade",
        description: "Blue Topaz Bangle 24K Gold",
        price: "789,09 $",
        imageUrl: anh57
    }
]
export default items