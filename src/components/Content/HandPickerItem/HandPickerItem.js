import React from 'react'
import "./HandPickerItem.css";
import items from '../../../info2';
import vecter9 from "../../../assets/images/Vector 9.png";
import vecter10 from "../../../assets/images/Vector 10.png";
import favorite from "../../../assets/images/icons8-favorite-30.png";

import image1 from "../../../assets/images/ABC/unsplash_M7cQqGjBNm8.png";
import image2 from "../../../assets/images/ABC/Rectangle 19.png";
import image3 from "../../../assets/images/ABC/Rectangle 21.png";
import image4 from "../../../assets/images/ABC/Rectangle 23.png";

const HandPickerItem = () => {
    return (
        <>
            <div className='handle-pickUp-item'>
                <div className='container'>
                    <div>
                        <h5 className='h5-discover'>Discover unique hand-picked items</h5>
                    </div>
                    <div className='div-form-card-items'>
                        {items.map((element) => {
                            return (
                                <>
                                    <div class="div-card-items" >
                                        <img class="card-img-top" src={element.imageUrl} alt="Card image cap" />
                                        <div class="card-body">
                                            <div className='body-p'>
                                                <p>{element.description}</p>
                                                <p className='p-name'>{element.name}</p>
                                            </div>
                                            <div>
                                                <h6>{element.price}</h6>
                                            </div>
                                        </div>
                                        <div className='div-icon-favorite'>
                                            <img src={favorite} alt="anh-favorite" className='icon-favorite'></img>
                                        </div>
                                        <div className='div-button-items'>
                                            <div className='button-front-items'>
                                                <img src={vecter9}></img>
                                            </div>
                                            <div className='button-back-items'>
                                                <img src={vecter10}></img>
                                            </div>
                                        </div>
                                    </div>
                                </>
                            )
                        })}
                    </div>
                </div>
                <div className='div-info-product'>
                    <div className='container'>
                        <div className='row'>
                            <div className='div-list'>
                                <p>Modern<br />Farmhouse</p>
                            </div>
                            <div className='div-list'>
                                <p>Eclectic<br />Decor</p>
                            </div>
                            <div className='div-list'>
                                <p>Polka Boho<br />Decor</p>
                            </div>
                            <div className='div-list'>
                                <p>Minimalist<br />Style</p>
                            </div>
                            <div className='div-list'>
                                <p>Anniversary<br />Gift</p>
                            </div>
                            <div className='div-list'>
                                <p>Wedding<br />Gifts</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div >
                <div className='container div-card-item-list'>
                    <div className='div-card-item-1'>

                    </div>
                    <div className='div-card-item-2'>
                        <div className='div-content-item-2-1'>
                            <img src={image2} className="image-2"></img>
                        </div>
                        <div className='div-content-item-2-2'>
                            <p >Colors</p>
                        </div>
                        <div className='div-content-item-2-3'>
                            <div className='div-color-1'>

                            </div>
                            <div className='div-color-2'>

                            </div>
                            <div className='div-color-3'>

                            </div>
                            <div className='div-color-4'>

                            </div>
                            <div className='div-color-5'>

                            </div>
                            <div className='div-color-6'>

                            </div>
                            <div className='div-color-7'>

                            </div>
                        </div>
                        <div className='div-content-item-2-4'>
                            <p>Rovena Riva Series 6 Pcs.<br></br> Wide Seating Claret Red Chair</p>
                            <div>
                                <h6>195,13 $</h6>
                            </div>
                        </div>
                    </div>
                    <div className='div-card-item-3'>
                        <div className='div-content-item-2-2'>
                            <img src={image4} className="image-3"></img>
                        </div>
                        <div className='div-content-item-2-2'>
                            <p >Colors</p>
                        </div>
                        <div className='div-content-item-2-3'>
                            <div className='div-color-1'>

                            </div>
                            <div className='div-color-4'>

                            </div>
                            <div className='div-color-5'>

                            </div>
                            <div className='div-color-6'>

                            </div>
                            <div className='div-color-7'>

                            </div>
                        </div>
                        <div className='div-content-item-2-4'>
                            <p>Iphone 12 128 GB (Red) Apple</p>
                            <div>
                                <h6>458,43 $</h6>
                            </div>
                        </div>
                    </div>
                    <div className='div-card-item-4'>
                        <div className='div-content-item-2-2'>
                            <img src={image3} className="image-3"></img>
                        </div>
                        <div className='div-content-item-4-2'>
                            <p >Colors</p>
                        </div>
                        <div className='div-content-item-2-3'>
                            <div className='div-color-4'>

                            </div>
                            <div className='div-color-5'>

                            </div>
                            <div className='div-color-6'>

                            </div>
                        </div>
                        <div className='div-content-item-2-4'>
                            <p>Rovena Riva Series 6 Pcs.<br></br> Wide Seating Claret Red Chair</p>
                            <div>
                                <h6>458,43 $</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='container div-form-bottom'>
                <div className='div-form-card-items'>
                        {items.map((element) => {
                            return (
                                <>
                                    <div class="div-card-items" >
                                        <img class="card-img-top" src={element.imageUrl} alt="Card image cap" />
                                        <div class="card-body">
                                            <div className='body-p'>
                                                <p>{element.description}</p>
                                                <p className='p-name'>{element.name}</p>
                                            </div>
                                            <div>
                                                <h6>{element.price}</h6>
                                            </div>
                                        </div>
                                        <div className='div-icon-favorite-1'>
                                            <img src={favorite} alt="anh-favorite" className='icon-favorite'></img>
                                        </div>
                                        <div className='div-button-items'>
                                            <div className='button-front-items'>
                                                <img src={vecter9}></img>
                                            </div>
                                            <div className='button-back-items'>
                                                <img src={vecter10}></img>
                                            </div>
                                        </div>
                                    </div>
                                </>
                            )
                        })}
                    </div>
            </div>
        </>
    )
}

export default HandPickerItem