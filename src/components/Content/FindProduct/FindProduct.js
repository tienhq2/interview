import React from 'react'
import "./FindProduct.css";
import products from '../../../info.js';
import vecter9 from "../../../assets/images/Vector 9.png";
import vecter10 from "../../../assets/images/Vector 10.png";
const FindProduct = () => {
    return (
        <div className='div-find-product'>
            <div className='container'>
                <div>
                    <h5 className='word-find'>Find thing you'll love. Support independent sellers</h5>
                </div>
                <div>
                    <p className='only-on'>Only on polka.</p>
                </div>
                <div className='div-form-image'>
                    {products.map((element) => {
                        return (
                            <div className='form-image'>
                                <div className='div-image'>
                                    <img src={element.imageUrl}></img>
                                </div>
                                <div>
                                    <h5 className='p-image'>{element.name}</h5>
                                </div>
                            </div>
                        )
                    })}
                </div>
                <div className='div-button-products'>
                    <div className='button-front-products'>
                        <img src={vecter9}></img>
                    </div>
                    <div className='button-back-products'>
                        <img src={vecter10}></img>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FindProduct