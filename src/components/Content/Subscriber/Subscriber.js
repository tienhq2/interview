import React from 'react'
import "./Subscriber.css"
const Subscriber = () => {
    return (
        <>
            <div className='container div-subscriber'>
                <h6>Yes!</h6>
                <div>
                    <p>Send me exclusive offers, unique gift ideas, and personalized<br />
                        tips for shopping and selling on Commerce.</p>
                </div>
                <div className='form-subsriber-input'>
                    <div className='form-input-subsriber'>
                        <div className='row'>
                            <div className='col-sm-9'>
                                <input placeholder='Drop your Email' className='input-email-sub'></input>
                            </div>
                            <div className='col-sm-3'>
                                <button className='btn-sub'>Subscribe &nbsp; <i class="fas fa-arrow-right"></i></button>
                            </div>
                        </div>
                        <div className='div-a-ready'>
                            <a href='#'>First order only. You're ready?</a>
                        </div>
                    </div>
                </div>
            </div>
            <div className='div-bottom'>
                <p className='p-div-bottom'>Commerce, is powered by 100% renewable electricity.</p>
            </div>
        </>
    )
}

export default Subscriber