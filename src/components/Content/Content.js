import React from 'react'
import HandPickerItem from './HandPickerItem/HandPickerItem'
import Carousel from './carousel/Carousel'
import FindProduct from './FindProduct/FindProduct'
import Subscriber from './Subscriber/Subscriber'

const Content = () => {
  return (
    <div>
        <Carousel/>
        <FindProduct/>
        <HandPickerItem/>
        <Subscriber/>
    </div>
  )
}

export default Content