import React from 'react'
import "./Carousel.css";
import vecter9 from "../../../assets/images/Vector 9.png";
import vecter10 from "../../../assets/images/Vector 10.png";
import anh1 from "../../../assets/images/unsplash_bU8TeXhsPcY.png";
import anh2 from "../../../assets/images/unsplash_esRJtEsvJhU.png";
import anh3 from "../../../assets/images/unsplash_m_7p45JfXQo.png";
const Carousel = () => {
    return (
        <div className='container'>
            <div className='div-carousel'>
                <div className='div-word'>
                    <h3>Whats New!</h3>
                    <p>Just million of people selling the things they love.</p>
                </div>
                <div className='div-button'>
                    <div className='button-front'>
                        <img src={vecter9}></img>
                    </div>
                    <div className='button-back'>
                        <img src={vecter10}></img>
                    </div>
                </div>
            </div>
            <div className='div-card'>
                <div className='row'>
                    <div className='col-sm-4'>
                        <div className='div-card-1'>
                            <div className='row'>
                                <b className='b'> A community <br /> doing good</b>
                                <h6>Commerce is a global online <br />marketplace, where people.</h6>
                            </div>
                            <div>
                                <img src={anh1} alt="anh-1"></img>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-4'>
                        <div className='div-card-2'>
                            <div className='row'>
                                <b className='b'> Support independent<br />creator</b>
                                <h6>Just millions of people selling<br />the things they love.</h6>
                            </div>
                            <div>
                                <img src={anh2} alt="anh-2"></img>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-4'>
                        <div className='div-card-3'>
                            <div className='row'>
                                <b className='b'>Peace<br />of mind</b>
                                <h6>Privacy is the hightestpriority<br />of our dedicated team.</h6>
                            </div>
                            <div>
                                <img src={anh3} alt="anh-3"></img>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default Carousel