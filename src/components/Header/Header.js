import React from 'react'
import "./Header.css"

const Header = () => {
    return (
        <div className='grid-header'>
            <div className='container'>
                <div className='row'>
                    <div className='col-sm-2'>
                        <h5 className='e-commerce'>E-commerce</h5>
                    </div>
                    <div className='col-sm-7 div-input'>
                        <div className='row'>
                            <div className='col-sm-3'>
                                <select className='select-catogories'>
                                    <option>All categories<i class="fas fa-home"></i></option>
                                    <option>All categories</option>
                                    <option>All categories</option>
                                    <option>All categories</option>
                                </select>
                            </div>
                            <div className='col-sm-8'>
                                <input placeholder='Search anything' className='input-search'></input>
                            </div>
                            <div className='col-sm-1'>
                                <button className='button-search' title='search'><i className='fas fa-search'></i></button>
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-3'>
                        <div className='row'>
                            <div className='col-sm-3'>
                                <button className='button-help'>Help</button>
                            </div>
                            <div className='col-sm-8 div-icon'>
                                <div className='row'>
                                    <div className='col-sm-1'>
                                        <i className='far fa-user'></i>
                                    </div>
                                    <div className='col-sm-4'>
                                        <h6>Account</h6>
                                    </div>
                                    <div className='col-sm-1'>
                                        <i className='fas fa-cart-shopping'></i>
                                    </div>
                                    <div className='col-sm-4'>
                                        <h6>Shoping</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='container'>
                <ul className='ul-header'>
                    <li><a href='#'>Jewelry & Accessories</a></li>
                    <li><a href='#'>Clothing & Shoe</a></li>
                    <li><a href='#'>Home & Living</a></li>
                    <li><a href='#'>Wedding & Party</a></li>
                    <li><a href='#'>Tops & Entertainment</a></li>
                    <li><a href='#'>Art & Collectibles</a></li>
                    <li><a href='#'>Craft Supplies & Tools</a></li>
                </ul>
            </div>
        </div>
    )
}

export default Header