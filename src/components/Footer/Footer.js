import React from 'react'
import "./Footer.css";

const Footer = () => {
    return (
        <div className='div-footer'>
            <div className='container'>
                <div className='row'>
                    <div className='col-sm-4'>
                        <h5 className='h5-ecomerce'>E-commerce</h5>
                        <p>Cricklewood,London<br />
                            NW2 6qg, Uk</p>
                        <div className='footer-icon'>
                            <i className='fab fa-facebook'></i>
                            <i className='fab fa-twitter'></i>
                            <i className='fab fa-linkedin'></i>
                            <i className='fab fa-dribbble'></i>
                        </div>
                    </div>
                    <div className='col-sm-8'>
                        <div className='row'>
                            <div className='col-sm-3'>
                                <b>Shop</b>
                                <div>
                                    <ul>
                                        <li><a href='#'>Gift cards</a></li>
                                        <li><a href='#'>Site map</a></li>
                                        <li><a href='#'>Polka blog</a></li>
                                        <li><a href='#'>Login</a></li>
                                        <li><a href='#'>Sign in</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className='col-sm-3'>
                                <b>Sell</b>
                                <div>
                                    <ul>
                                        <li><a href='#'>Sell on Polka</a></li>
                                        <li><a href='#'>Teams</a></li>
                                        <li><a href='#'>Forums</a></li>
                                        <li><a href='#'>Affiliates</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className='col-sm-3'>
                                <b>About</b>
                                <div>
                                    <ul>
                                        <li><a href='#'>Polka, Inc.</a></li>
                                        <li><a href='#'>Policies</a></li>
                                        <li><a href='#'>Investors</a></li>
                                        <li><a href='#'>Careers</a></li>
                                        <li><a href='#'>Press</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className='col-sm-3'>
                                <b>Help</b>
                                <div>
                                    <ul>
                                        <li><a href='#'>Help Center</a></li>
                                        <li><a href='#'>Trust and safety</a></li>
                                        <li><a href='#'>Privacy settings    </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='container div-footer-bottom'>
                <div className='row'>
                    <div className='col-sm-4'>
                        <p>© 2022 Commerce, Inc.</p>
                    </div>
                    <div className='col-sm-8 div-terms'>
                        <a href='#'>Privacy policy</a>
                        <a href='#' className='a-terms'>Terms of use</a>
                        <a href='#'>Cookies</a>
                        <button className='button-scroll'>Scroll to top &nbsp; <i className="fas fa-arrow-up"></i></button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer