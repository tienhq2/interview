import Header from "./components/Header/Header";
import "bootstrap/dist/css/bootstrap.min.css";
import Content from "./components/Content/Content";
import Footer from "./components/Footer/Footer";
function App() {
  return (
    <div >
      <Header/>
      <Content/>
      <Footer/>
    </div>
  );
}

export default App;
