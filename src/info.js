import anh25 from "./assets/images/luu niem/Rectangle 25.png";
import anh27 from "./assets/images/luu niem/Rectangle 27.png";
import anh29 from "./assets/images/luu niem/Rectangle 29.png";
import anh31 from "./assets/images/luu niem/Rectangle 31.png";
import anh33 from "./assets/images/luu niem/Rectangle 33.png";
import anh35 from "./assets/images/luu niem/Rectangle 35.png";
import anh37 from "./assets/images/luu niem/Rectangle 37.png";
import anh39 from "./assets/images/luu niem/Rectangle 39.png";


const products = [
    {
        id:1,
        name: "Craft Kits",
        imageUrl: anh25
    },
    {
        id:2,
        name: "Throw Pillows",
        imageUrl: anh27
    },
    {
        id:3,
        name: "Natural Glass",
        imageUrl: anh29
    },
    {
        id:4,
        name: "Self-care",
        imageUrl: anh31
    },
    {
        id:5,
        name: "Gift ideas",
        imageUrl: anh33
    },
    {
        id:6,
        name: "Wall Decor",
        imageUrl: anh35
    },
    {
        id:7,
        name: "Wedding",
        imageUrl: anh37
    },
    {
        id:8,
        name: "Lift Style",
        imageUrl: anh39
    },
]

export default 
    products

